'use strict';

angular.module('liquidCalc')
  .directive('higherThen',
    function ()
    {
      return {
        require: "ngModel",
        scope: {
          otherModelValue: "=higherThen"
        },
        link: function (scope, element, attributes, ngModel)
        {
          ngModel.$validators.higherThen = function (modelValue)
          {
            if (modelValue !== undefined) {
              return scope.otherModelValue >= modelValue;
            }

            return true;
          };

          scope.$watch("otherModelValue", function ()
          {
            ngModel.$validate();
          });
        }
      };
    });