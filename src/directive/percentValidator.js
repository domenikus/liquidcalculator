'use strict';

angular.module('liquidCalc')
  .directive('validatePercentTo',
    function ()
    {
      return {
        require: "ngModel",
        scope: {
          otherModelValue: "=validatePercentTo"
        },
        link: function (scope, element, attributes, ngModel)
        {
          ngModel.$validators.validatePercent = function (modelValue)
          {
            if (modelValue !== undefined) {
              var percent = modelValue + scope.otherModelValue;
              return percent <= 100;
            }

            return true;
          };

          scope.$watch("otherModelValue", function ()
          {
            ngModel.$validate();
          });
        }
      };
    });