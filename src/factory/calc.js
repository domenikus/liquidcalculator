'use strict';

var app = angular.module('liquidCalc');

app.factory('calc', function () {
  var factory = {};

  factory.calcPercent = function(amount, percent) {
    var result = {};
    result.percentValue = percent * amount / 100;
    result.subtractAmount = amount - result.percentValue;

    return result;
  };

  factory.calcNicotineFactor = function (nicotineStrength) {
    return (1 / nicotineStrength);
  };

  return factory;
});
