'use strict';

var app = angular.module('liquidCalc');

app.controller('mixBaseController', ['$rootScope', '$scope', 'calc', '$localStorage',
  function ($rootScope, $scope, calc, $localStorage) {

    this.mix = function (noHistory) {
      if (noHistory === undefined) {
        noHistory = false;
      }

      var pg = calc.calcPercent(this.baseAmount, this.pgPercentage);
      var vg = calc.calcPercent(this.baseAmount, this.vgPercentage);
      var water = calc.calcPercent(this.baseAmount, (100 - this.pgPercentage - this.vgPercentage));
      var nicotine = 0;

      if (this.withNicotine) {
        nicotine = parseFloat((this.baseAmount * calc.calcNicotineFactor($localStorage.globalNicotineStrength) * this.nicotineStrength).toFixed(2));
      }

      this.pgAmount = parseFloat((pg.percentValue - nicotine / 2).toFixed(2));
      this.vgAmount = parseFloat((vg.percentValue - nicotine / 2).toFixed(2));
      this.waterAmount = water.percentValue;
      this.nicotineAmount = nicotine;

      if (!noHistory) {
        this.saveToHistory();
      }
    };

    this.clearForm = function () {
      this.nicotineStrength = "";

      this.pgAmount = "";
      this.vgAmount = "";
      this.waterAmount = "";
      this.nicotineAmount = "";

      $scope.mb_form.$setPristine();
      $scope.mb_form.$setUntouched();
    };

    this.restoreFromHistory = function (index) {
      var values = $localStorage.mb_history[index];
      this.baseAmount = values.baseAmount;
      this.pgPercentage = values.pg;
      this.vgPercentage = values.vg;
      this.waterAmount = values.water;
      this.nicotineStrength = values.nicotine;

      if (values.nicotine === undefined || values.nicotine === "") {
        this.withNicotine = false;
      } else {
        this.withNicotine = true;
      }

      this.mix(true);
    };

    this.saveToHistory = function () {
      if ($localStorage.mb_history === undefined) {
        $localStorage.mb_history = [];
      }

      var item = {
        baseAmount: this.baseAmount,
        pg: this.pgPercentage,
        vg: this.vgPercentage,
        water: this.waterAmount,
        nicotine: this.nicotineStrength
      };

      if ($localStorage.mb_history.indexOf(item) === -1) {
        if ($localStorage.mb_history.length > 9) {
          $localStorage.mb_history.splice(-1, 1);
        }
        $localStorage.mb_history.unshift(item);
      }
    };
  }]);