'use strict';

var app = angular.module('liquidCalc');

app.controller('settingsController', ['$localStorage', '$translate', '$mdDialog',
  function ($localStorage, $translate, $mdDialog) {
    console.log('controller');
    this.deleteHistory = function () {
      console.log('function');
      var title = "";
      var content = "";
      var ok = "";
      var cancel = "";

      $translate(['SETTINGS.D_DELETE_HISTORY_TITLE', 'SETTINGS.D_DELETE_HISTORY_CONTENT', 'SETTINGS.D_DELETE_HISTORY_OK', 'SETTINGS.D_DELETE_HISTORY_CANCEL']).then(function (translations) {
        title = translations['SETTINGS.D_DELETE_HISTORY_TITLE'];
        content = translations['SETTINGS.D_DELETE_HISTORY_CONTENT'];
        ok = translations['SETTINGS.D_DELETE_HISTORY_OK'];
        cancel = translations['SETTINGS.D_DELETE_HISTORY_CANCEL'];
      }).finally(function () {
        var confirm = $mdDialog.confirm()
          .title(title)
          .textContent(content)
          .ariaLabel(title)
          .ok(ok)
          .cancel(cancel);
        $mdDialog.show(confirm).then(function () {
          if ($localStorage.ml_history !== undefined) {
            delete $localStorage.ml_history;
          }

          if ($localStorage.mb_history !== undefined) {
            delete $localStorage.mb_history;
          }

          if ($localStorage.mlsav_history !== undefined) {
            delete $localStorage.mlsav_history;
          }
        });
      });
    };
  }]);