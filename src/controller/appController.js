'use strict';

var app = angular.module('liquidCalc');

app.controller('appController', ['$rootScope', '$scope', '$location', '$localStorage', '$translate',
  function ($rootScope, $scope, $location, $localStorage, $translate) {

    $rootScope.globalBaseNicotineStrength = $localStorage.globalNicotineStrength;

    $rootScope.setCurrentNavItemByUrl = function (withSubroutes) {
      if ($location.path() !== '') {
        var route = null;
        if (withSubroutes) {
          route = $location.path().substring(1);
        } else {
          route = ($location.path().substring(1)).split('/')[0];
        }
        return route;
      } else {
        if (withSubroutes) {
          return "mixLiquid/simple";
        } else {
          return "mixLiquid";
        }
      }
    };

    $scope.$watch('language', function (language) {
      if (language !== undefined) {
        $translate.use(language);
        $localStorage.language = language;
      }
    });
  }]);
