'use strict';

var app = angular.module('liquidCalc');

app.controller('calcController', ['$scope', '$localStorage', 'calc', '$mdDialog', '$translate', '$mdToast', '$location',
  function ($scope, $localStorage, calc, $mdDialog, $translate, $mdToast, $location) {

    // Workaround to access forms in parent controller
    $scope.forms = {};

    // $scope.setCurrentNavItemByUrl = function () {
    //   if ($location.path() !== '') {
    //     $scope.currentNavItem = $location.path().substring(1);
    //   } else {
    //     $scope.currentNavItem = 'mixLiquid.simple';
    //   }
    // };

    // $scope.setCurrentPage = function (page) {
    //   $scope.page = page;
    // };

    // var calcNicotineFactor = function (nicotineStrength) {
    //   return (1 / nicotineStrength);
    // };



    // $scope.$watch('language', function (language) {
    //   if (language !== undefined) {
    //     $translate.use(language);
    //     $localStorage.language = language;
    //   }
    // });

    $scope.mixLiquid = function (noHistory) {

    };

    $scope.mixLiquidExtended = function () {
      var result;
      var baseAmount = $scope.mle_baseAmount;

      do {
        result = calc.calcPercent(baseAmount, $scope.mle_flavourPercentage);
        baseAmount = baseAmount - 1;
      } while (result.percentValue > $scope.mle_flavourAmount);


      var liquidTotal = result.subtractAmount + result.percentValue;
      var addFlavour = result.percentValue;

      $scope.mle_liquidAmount = liquidTotal;
      $scope.mle_addFlavourAmount = addFlavour;

      if ($scope.mle_withNicotine) {
        $scope.mle_addNicotineAmount = parseFloat((result.subtractAmount * calcNicotineFactor($localStorage.globalNicotineStrength) * $scope.mle_nicotineStrength).toFixed(2));
        $scope.mle_addBaseAmount = parseFloat((result.subtractAmount - $scope.mle_addNicotineAmount).toFixed(2));
      } else {
        $scope.mle_addBaseAmount = parseFloat((result.subtractAmount).toFixed(2));
      }
    };





    $scope.clearMlForm = function () {
      $scope.ml_nicotineStrength = "";
      $scope.ml_additionalFlavourPercentage = "";

      $scope.ml_baseAmount = "";
      $scope.ml_flavourAmount = "";
      $scope.ml_nicotineAmount = "";
      $scope.ml_additionalFlavourAmount = "";
      $scope.forms.ml_form.$setPristine();
      $scope.forms.ml_form.$setUntouched();
    };

    $scope.clearMleForm = function () {
      $scope.mle_liquidAmount = "";
      $scope.mle_addBaseAmount = "";
      $scope.mle_addFlavourAmount = "";
      $scope.mle_addNicotineAmount = "";
    };

  }]);