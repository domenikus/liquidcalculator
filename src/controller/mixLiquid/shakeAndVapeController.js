'use strict';

var app = angular.module('liquidCalc');

app.controller('mixLiquidShakeAndVapeController', ['$scope', '$localStorage', 'calc', '$mdDialog', '$translate',
  function ($scope, $localStorage, calc, $mdDialog, $translate) {

    this.mix = function (noHistory) {
      if (noHistory === undefined) {
        noHistory = false;
      }

      var nicotineAmount = parseFloat(((this.nicotineStrength * this.overdosedLiquidSize) / ($localStorage.globalNicotineStrength - this.nicotineStrength)).toFixed(2));
      var liquidAmount = Math.ceil((this.overdosedLiquidSize + nicotineAmount) / 10) * 10;

      this.liquidAmount = liquidAmount;
      this.addBaseAmount = parseFloat((liquidAmount - nicotineAmount - this.overdosedLiquidSize).toFixed(2));
      this.addNicotineAmount = nicotineAmount;

      if (!noHistory) {
        this.saveToHistory();
      }
    };

    this.saveToHistory = function () {
      if ($localStorage.mlsav_history === undefined) {
        $localStorage.mlsav_history = [];
      }

      var item = {
        liquidAmount: this.overdosedLiquidSize,
        nicotine: this.nicotineStrength

      };

      if ($localStorage.mlsav_history.indexOf(item) === -1) {
        if ($localStorage.mlsav_history.length > 9) {
          $localStorage.mlsav_history.splice(-1, 1);
        }
        $localStorage.mlsav_history.unshift(item);
      }
    };

    this.restoreFromHistory = function (index) {
      var item = $localStorage.mlsav_history[index];

      this.overdosedLiquidSize = item.liquidAmount;
      this.nicotineStrength = item.nicotine;

      this.mix(true);
    };
  }]);