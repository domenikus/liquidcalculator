'use strict';

var app = angular.module('liquidCalc');

app.controller('mixLiquidExtendedController', ['$scope', '$localStorage', 'calc',
  function ($scope, $localStorage, calc) {

    this.mix = function () {
      var result;
      var baseAmount = this.baseAmount;

      do {
        result = calc.calcPercent(baseAmount, this.flavourPercentage);
        baseAmount = baseAmount - 1;
      } while (result.percentValue > this.flavourAmount);


      var liquidTotal = result.subtractAmount + result.percentValue;
      var addFlavour = result.percentValue;

      this.liquidAmount = liquidTotal;
      this.addFlavourAmount = addFlavour;

      if (this.withNicotine) {
        this.addNicotineAmount = parseFloat((result.subtractAmount * calc.calcNicotineFactor($localStorage.globalNicotineStrength) * this.nicotineStrength).toFixed(2));
        this.addBaseAmount = parseFloat((result.subtractAmount - this.addNicotineAmount).toFixed(2));
      } else {
        this.addBaseAmount = parseFloat((result.subtractAmount).toFixed(2));
      }
    };

    this.clearForm = function () {
      this.nicotineStrength = "";
      this.liquidAmount = "";
      this.addBaseAmount = "";
      this.addFlavourAmount = "";
      this.addNicotineAmount = "";
    };
  }]);