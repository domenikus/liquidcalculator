'use strict';

var app = angular.module('liquidCalc');

app.controller('mixLiquidSimpleController', ['$scope', '$localStorage', 'calc',
  function ($scope, $localStorage, calc) {

  this.mix = function (noHistory) {
    if (noHistory === undefined) {
      noHistory = false;
    }

    var mixture = calc.calcPercent(this.liquidAmount, (this.flavourPercentage + this.additionalFlavourPercentage));
    var flavour = calc.calcPercent(this.liquidAmount, this.flavourPercentage);

    this.flavourAmount = parseFloat((flavour.percentValue).toFixed(2));

    if (this.withAdditionalFlavour) {
      this.additionalFlavourAmount = parseFloat((mixture.percentValue - flavour.percentValue).toFixed(2));
    }

    if (this.withNicotine) {
      this.nicotineAmount = parseFloat((this.liquidAmount * calc.calcNicotineFactor($localStorage.globalNicotineStrength) * this.nicotineStrength).toFixed(2));
      this.baseAmount = parseFloat((mixture.subtractAmount - this.nicotineAmount).toFixed(2));
    } else {
      this.baseAmount = parseFloat((flavour.subtractAmount).toFixed(2));
    }

    if (!noHistory) {
      this.saveToHistory();
    }
  };

  this.saveToHistory = function () {
    if ($localStorage.ml_history === undefined) {
      $localStorage.ml_history = [];
    }

    var item = {
      liquidAmount: this.liquidAmount,
      flavourPercentage: this.flavourPercentage,
      additionalFlavourPercentage: this.additionalFlavourPercentage,
      nicotineStrength: this.nicotineStrength
    };

    if ($localStorage.ml_history.indexOf(item) === -1) {
      if ($localStorage.ml_history.length > 9) {
        $localStorage.ml_history.splice(-1, 1);
      }
      $localStorage.ml_history.unshift(item);
    }
  };

    this.restoreFromHistory = function (index) {
      var values = $localStorage.ml_history[index];
      this.liquidAmount = values.liquidAmount;
      this.flavourPercentage = values.flavourPercentage;
      this.additionalFlavourPercentage = values.additionalFlavourPercentage;
      this.nicotineStrength = values.nicotineStrength;

      if (values.additionalFlavourPercentage == undefined || values.additionalFlavourPercentage === "") {
        this.withAdditionalFlavour = false;
      } else {
        this.withAdditionalFlavour = true;
      }

      if (values.nicotineStrength == undefined || values.nicotineStrength === "") {
        this.withNicotine = false;
      } else {
        this.withNicotine = true;
      }

      this.mix(true);
    };

    this.clearForm = function () {
      this.nicotineStrength = "";
      this.additionalFlavourPercentage = "";

      this.baseAmount = "";
      this.flavourAmount = "";
      this.nicotineAmount = "";
      this.additionalFlavourAmount = "";
      $scope.ml_form.$setPristine();
      $scope.ml_form.$setUntouched();
    };
}]);