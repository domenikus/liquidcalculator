var gulp = require('gulp');
var mainBowerFiles = require('main-bower-files');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var bower = require('gulp-bower');
var cssnano = require('gulp-cssnano');
var del = require('del');
var htmlmin = require('gulp-htmlmin');

gulp.task( 'default', [ 'clean', 'vendor-bundle', 'app-bundle', 'css', 'index', 'html', 'img', 'i18n', 'htaccess' ] );

gulp.task('bower-restore', function () {
  return bower();
});

gulp.task('clean', function(){
  return del(['dist/*'], {force:true});
});

gulp.task('vendor-bundle', ['clean', 'bower-restore', 'app-bundle'], function () {
  return gulp.src(mainBowerFiles({ filter: '**/*.js' }))
    .pipe(concat('vendors.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
});

gulp.task('app-bundle', ['clean'], function () {
  return gulp.src(
    [
      'app.js',
      'src/**/*.js',
      'src/**/**/*.js'
    ]
  )
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
});

gulp.task('index', ['clean'], function () {
  gulp.src('index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'));
});

gulp.task('html', ['clean'], function () {
  gulp.src(['src/view/*.html', 'src/view/**/*.html'])
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist/html'));
});

gulp.task("css", ['clean', "bower-restore"], function () {
  return gulp.src(mainBowerFiles({ filter: '**/*.css' }).concat('css/*.css'))
    .pipe(concat('site.min.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('img', ['clean'], function () {
  gulp.src('images/*')
    .pipe(gulp.dest('dist/img'));
});

gulp.task('i18n', ['clean'], function () {
  gulp.src('i18n/*')
    .pipe(gulp.dest('dist/i18n'));
});

gulp.task('htaccess', ['clean'], function () {
    gulp.src('\\.htaccess')
        .pipe(gulp.dest('dist'));
});
