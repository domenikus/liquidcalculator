# Liquidcalculator #

Calculations for Liquid mixer

## Features ##

* Mix Liquid with and without nicotine and add extra flavour
* Extend existing liquids with and without nicotine and extra flavour
* Shake and Vape calculator
* Mix Bases

## Language support ##

* If you want to use the calculator in your language download the 'i18n/en.json' file, translate it and send it via email oder create a pull request. 

## Install ##

```
$ npm install
$ bower install
$ gulp
```

* Copy ./dist folder on your webserver

## Contributing ##

* Feel free to submit [Bitbucket Issues](https://bitbucket.org/domenikus/liquidcalculator/issues) or pull requests
