'use strict';

var app = angular.module('liquidCalc', [
  'ngMaterial',
  'ui.router',
  'ngMessages',
  'ngStorage',
  'pascalprecht.translate'
]);

app.config(['$localStorageProvider', '$translateProvider', '$qProvider', '$stateProvider', '$urlRouterProvider',
  function ($localStorageProvider, $translateProvider, $qProvider, $stateProvider, $urlRouterProvider) {
    $localStorageProvider.setKeyPrefix('');
    $qProvider.errorOnUnhandledRejections(false);

    $translateProvider.useSanitizeValueStrategy(null);

    var time = new Date().getTime();
    $translateProvider.useStaticFilesLoader({
      prefix: 'i18n/',
      suffix: '.json?time=' + time
    });
    $translateProvider.preferredLanguage('us');

    // Routing
    $urlRouterProvider.otherwise("/mixLiquid/simple");
    $stateProvider
        .state('mixLiquid', {
          abstract: true,
          url: "/mixLiquid",
          templateUrl: "html/mixLiquid/mixLiquid.html?time=" + time,
          controller: "mixLiquidController as mlCtrl"
      }).state('mixLiquid.simple', {
          url: "/simple",
          templateUrl: "html/mixLiquid/simple.html?time=" + time,
          controller: "mixLiquidSimpleController as simpleCtrl"
      }).state('mixLiquid.shakeAndVape', {
          url: "/shakeAndVape",
          templateUrl: "html/mixLiquid/shakeAndVape.html?time=" + time,
          controller: "mixLiquidShakeAndVapeController as shakeAndVapeCtrl"
      }).state('mixLiquid.extended', {
          url: "/extended",
          templateUrl: "html/mixLiquid/extended.html?time=" + time,
          controller: "mixLiquidExtendedController as extendedCtrl"
      }).state({
          name: "mixBase",
          url: "/mixBase",
          templateUrl: "html/mixBase.html?time=" + time,
          controller: "mixBaseController as mixBaseCtrl"
      }).state({
          name: "settings",
          url: "/settings",
          templateUrl: "html/settings.html?time=" + time,
          controller: "settingsController as settingsCtrl"
      }).state({
          name: "info",
          url: "/info",
          templateUrl: "html/info.html?time=" + time
      }).state({
          name: "impressum",
          url: "/impressum",
          templateUrl: "html/impressum.html?time=" + time
    });
  }]);

app.run(['$rootScope', '$localStorage', '$translate', function ($rootScope, $localStorage, $translate) {
  $rootScope.$storage = $localStorage;

  var userLang;
  if ($localStorage.language !== undefined && $localStorage.language !== "") {
    userLang = $localStorage.language;
  } else {
    var browserLang = navigator.language || navigator.userLanguage;

    switch (browserLang) {
      case 'de-DE':
        userLang = 'de';
        break;
      default:
        userLang = 'us';
    }
  }

  if ($localStorage.globalNicotineStrength === undefined) {
    $localStorage.globalNicotineStrength = 20;
  }

  $translate.use(userLang);
  $rootScope.language = userLang;
}]);
